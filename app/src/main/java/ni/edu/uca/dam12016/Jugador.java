package ni.edu.uca.dam12016;

/**
 * Created by pwk04 on 10-17-16.
 */
public class Jugador {

    private String nombre;
    private int puntaje;


    public Jugador(){}

    public Jugador(String nombre, int puntaje){
        this.nombre = nombre;
        this.puntaje = puntaje;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
}
